package com.techu.apirest.controller;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}" )
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

    @PutMapping("/productos/{id}")
    public void putProductos(@RequestBody ProductoModel productoToUpdate){
        productoService.save(productoToUpdate);
    }

    @DeleteMapping("/productos{id}")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete){
        return productoService.delete(productoToDelete);
    }

    /*

    @PatchMapping("/productos/{id}")
    public ProductoModel patchProductos(@RequestBody ProductoModel productoToUpdate, @PathVariable int id){
        return (new ProductoModel(0, "null", 0.0));
    }

    */
}
